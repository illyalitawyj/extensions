using System;
using System.Collections.Generic;
using System.Linq;
using JetBrains.Annotations;
using Sirenix.Utilities;
using UnityEngine;
using Object = UnityEngine.Object;

namespace Extensions
{
    public static class GameObjectExtensions
    {
        public static GameObject Instantiate(this GameObject source,
            Transform parent = null, Vector3? localScale = null)
        {
            var instance = parent != null
                ? Instantiate(source, parent, true)
                : Instantiate(source);
            
            instance.transform.localPosition = Vector3.zero;
            instance.transform.localScale = localScale ?? Vector3.one;
            instance.name = instance.name.Replace("(Clone)", string.Empty);
            return instance;
        }

        public static List<GameObject> InstantiateEach(this IEnumerable<GameObject> sourceCollection,
            Transform parent = null, Vector3? localScale = null) =>
                sourceCollection
                    .Select(o => o.Instantiate(parent, localScale))
                    .ToList();
    }
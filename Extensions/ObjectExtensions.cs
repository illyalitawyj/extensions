using System;
using System.Collections.Generic;

namespace Extensions
{
    public static class ObjectExtensions
    {
        public static bool Exists<T>(this T @object) where T : class =>
            @object != null;

        public static bool Exist<T>(this T @object) where T : class =>
            @object != null;

        public static bool IsNull<T>(this T @object) where T : class =>
            @object == null;

        public static bool IsNotNull<T>(this T @object) where T : class =>
            @object != null;


        public static List<T> GetEnumMembers<T>(this Type type) where T : struct, Enum =>
            type.GetEnumNames().ToEnums<T>();
    }
}
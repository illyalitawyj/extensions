﻿using System.Linq;
using UnityEngine;

namespace Extensions
{
    public static class AnimatorExtensions
    {
        public static AnimationClip GetClip(this Animator animator, string targetName)
        {
            var clips = animator.runtimeAnimatorController.animationClips;
            return clips.FirstOrDefault(clip => clip.name == targetName);
        }
        
        public static float GetAnimationDurationByIndex(this Animator animator, int index)
        {
            var clips = animator.runtimeAnimatorController.animationClips;
            return clips != null && clips.Length > 0
                ? clips[index].length
                : 0f;
        }
    }
} 
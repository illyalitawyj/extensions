﻿using UnityEngine;

namespace Extensions
{
    public static class TransformExtensions
    {
        public static Transform SetX(this Transform self, float x)
        {
            var position = self.position;
            self.position = new Vector3(x, position.y, position.z);
            return self;
        }

        public static Transform SetY(this Transform self, float y)
        {
            var position = self.position;
            self.position = new Vector3(position.x, y, position.z);
            return self;
        }

        public static Transform SetZ(this Transform self, float z)
        {
            var position = self.position;
            self.position = new Vector3(position.x, position.y, z);
            return self;
        }

        public static Transform SetLocalX(this Transform self, float x)
        {
            var localPosition = self.localPosition;
            self.localPosition = new Vector3(x, localPosition.y, localPosition.z);
            return self;
        }

        public static Transform SetLocalY(this Transform self, float y)
        {
            var localPosition = self.localPosition;
            self.localPosition = new Vector3(localPosition.x, y, localPosition.z);
            return self;
        }

        public static Transform SetLocalZ(this Transform self, float z)
        {
            var localPosition = self.localPosition;
            self.localPosition = new Vector3(localPosition.x, localPosition.y, z);
            return self;
        }

        public static Transform SetLocalScale(this Transform self, float size)
        {
            self.localScale = new Vector3(size, size, size);
            return self;
        }

        public static Transform SetLocalScale(this Transform self, Vector3 newScale)
        {
            self.localScale = new Vector3(newScale.x, newScale.y, newScale.z);
            return self;
        }

        public static Transform ResetLocalPosition(this Transform self, Vector3 newPosition)
        {
            self.localPosition = new Vector3(newPosition.x, newPosition.y, newPosition.z);
            return self;
        }
    }
}
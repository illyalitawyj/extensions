using System;
using System.Collections.Generic;
using System.Linq;

namespace Extensions
{
    public static class StringExtensions
    {
        public static List<T> ToEnums<T>(this IEnumerable<string> names) where T : struct, Enum
            => names.Select(name => ToEnum<T>(name)).ToList();

        public static T ToEnum<T>(this string name) where T : struct, Enum
        {
            Enum.TryParse(name, out T @enum);
            return @enum;
        }

        public static bool IsNullOrEmpty(this string self) =>
            string.IsNullOrEmpty(self);

        public static bool IsNumber(this string @string)
        {
            return int.TryParse(@string, out var number);
        }

        public static bool IsLetter(this string @string)
        {
            return (!@string.IsNumber() && @string.Length < 2);
        }
        
        public static List<string> SplitWord(this string word)
        {
            return word.Select(letter => letter.ToString()).ToList();
        }
    }
}
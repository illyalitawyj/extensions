﻿using UnityEngine;

 namespace Extensions
{
    public static class FloatPowerExtensions
    {
        public static float Power(this float number, float power) =>
            Mathf.Pow(number, power);
        
        public static float SecondPower(this float number) =>
            Mathf.Pow(number, 2);
        
        public static float ThirdPower(this float number) =>
            Mathf.Pow(number, 3);
        
        public static float FourthPower(this float number) =>
            Mathf.Pow(number, 3);
    }
}